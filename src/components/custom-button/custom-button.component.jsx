import "./custom-button.styles.scss";

import React from "react";

const CustomButton = ({
 children,
 isGoogleSignIn,
 inverted,
 ...otherProps
}) => {
 return (
  <div
   className={`${inverted ? "inverted" : ""}   ${
    isGoogleSignIn ? "google-sign-in" : ""
   } custom-button`}
   {...otherProps}
  >
   {children}
  </div>
 );
};

export default CustomButton;
