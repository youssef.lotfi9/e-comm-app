import StripeCheckout from "react-stripe-checkout";

import React from "react";

function StripeCheckoutButton({ price }) {
 const priceForStripe = price * 100;
 const publishableKey =
  "pk_test_51IlGbeDMxApR9f5nKUZqlvblkkJidyfF2gJkWpsTd5BkY409JUQ1WTRw7cTR7swYKVHQfimiosOQJDkIytOELIL40070Asg8bi";
 const onToken = (token) => {
  alert("payment Successful :" + token);
 };
 return (
  <StripeCheckout
   label="Pay Now"
   name="Crown clothing LTD."
   allowRememberMe
   //  data-key="pk_test_51IlGbeDMxApR9f5nKUZqlvblkkJidyfF2gJkWpsTd5BkY409JUQ1WTRw7cTR7swYKVHQfimiosOQJDkIytOELIL40070Asg8bi"
   billingAddress
   shippingAddress
   image="https://sendeyo.com/up/d/f3eb2117da"
   description={`your total is : ${price}`}
   amount={priceForStripe}
   panelLabel="Pay Now"
   token={onToken}
   stripeKey={publishableKey}
  />
 );
}

export default StripeCheckoutButton;
