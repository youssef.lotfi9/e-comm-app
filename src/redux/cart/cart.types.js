const cartActionTypes = {
 TOGGLE_CART_HIDDEN: "TOGGLE_CART_HIDDEN",
 ADD_ITEM: "ADD_ITEM",
 CLEAT_ITEM_FROM_CART: "CLEAT_ITEM_FROM_CART",
};

export default cartActionTypes;
