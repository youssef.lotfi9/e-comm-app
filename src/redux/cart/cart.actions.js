import cartActionTypes from "./cart.types";

export const toggleCartHidden = () => ({
 type: cartActionTypes.TOGGLE_CART_HIDDEN,
});

export const addItem = (item) => ({
 type: cartActionTypes.ADD_ITEM,
 payload: item,
});

export const cleatItemFromCart = (item) => ({
 type: cartActionTypes.CLEAT_ITEM_FROM_CART,
 payload: item,
});
