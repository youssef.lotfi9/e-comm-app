import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

const config = {
 apiKey: "AIzaSyB6WFnlx5JGzObF7c79_iwizX_Oj2t3-AY",
 authDomain: "crown-db-fbc47.firebaseapp.com",
 projectId: "crown-db-fbc47",
 storageBucket: "crown-db-fbc47.appspot.com",
 messagingSenderId: "136745812833",
 appId: "1:136745812833:web:d194c00139a84cb6367b20",
};

export const createUserProfileDocument = async (userAuth, additionalData) => {
 if (!userAuth) return;

 const userRef = firestore.doc(`users/${userAuth.uid}`);
 const snapShot = await userRef.get();
 console.log(snapShot);

 if (!snapShot.exists) {
  const { displayName, email } = userAuth;
  const createAt = new Date();

  try {
   await userRef.set({
    displayName,
    email,
    createAt,
    ...additionalData,
   });
  } catch (error) {
   console.log("Error creating", error.message);
  }
 }

 return userRef;
};

firebase.initializeApp(config);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
const provider = new firebase.auth.GoogleAuthProvider();

provider.setCustomParameters({ prompt: "select_account" });

export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;
